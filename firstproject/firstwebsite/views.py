from django.shortcuts import render
from firstwebsite.models import Product,Category,Registration
from django.http import HttpResponse,HttpResponseRedirect
from django.contrib.auth import login,logout,authenticate
from django.contrib.auth.decorators import login_required
all_cat = Category.objects.all().order_by('c_name')
   
# Create your views here.

def indexpage(request):
    return render(request,'Home.html')

from django.contrib.auth.models import User
def stdup(request):
    if request.method=="POST":
        fst = request.POST['first']
        lst = request.POST['second']
        un = request.POST['un']
        email = request.POST['em']
        pwd = request.POST['pas']
        cn = request.POST['con']
        usrn = User.objects.create_user(un,email,pwd)
        usrn.first_name = fst
        usrn.last_name = lst
        usrn.is_active = True
        usrn.save()

        rg = Registration(user=usrn,contact=cn)
        rg.save()
    if'pic'in request.FILES:
           pic = request.FILES['pic']
           rg.profile_pic = pic
           rg.save()

    return render(request,'Signupstudents.html')
def checkUserExist(r):
        if r.method=='GET':
            un=r.GET['usern']
            chk = User.objects.filter(username=un)
            if len(chk)>0:
                return HttpResponse(1)
            else:
                return HttpResponse(0)    

def loginpage(request):
    all = Product.objects.all().order_by('id')
    if 'p_id' in request.GET:
        i = request.GET['p_id']
        all = Product.objects.filter(cat__id=i)
    return render(request,'login.html',{'p':all,'total':len(all),'cat':all_cat})
def stdpage(request):
    if 'userid' in request.COOKIES:
        uid = request.COOKIES['userid']
        usr = User.objects.get(id=uid)
        login(request,usr)
        return HttpResponseRedirect('/admin')
    if request.method=="POST":
        un = request.POST['usern']
        pwd = request.POST['pass']

        check = authenticate(username=un,password=pwd)
        if check:
            if check.is_superuser:
                login(request,check)
                return HttpResponseRedirect('/admin')
            if check.is_staff:
                login(request,check)
                res = HttpResponseRedirect('/job')
                res.set_cookie('userid',check.id)
                return res 
            if check.is_active:
                login(request,check)
                res = HttpResponseRedirect('/login')
                res.set_cookie('userid',check.id)
                return res    
            
        else:
            return render(request,'Student.html',{'status':'Invalid Login details!!'})
    return render(request,'Student.html')        
    
    
def resumepage(request):
    return render(request,'resume.html')
from django.contrib.auth.models import User
def signup(request):
    if request.method=="POST":
        fst = request.POST['first']
        lst = request.POST['last']
        un = request.POST['un']
        email = request.POST['em']
        pwd = request.POST['pas']
        cn = request.POST['con']
        usr = User.objects.create_user(un,email,pwd)
        usr.first_name = fst
        usr.last_name = lst
        usr.is_staff = True
        usr.save()

        rg = Registration(user=usr,contact=cn)
        rg.save()
    if'pic'in request.FILES:
           pic = request.FILES['pic']
           rg.profile_pic = pic
           rg.save()

    return render(request,'SignUp.html')
def checkUserExist(r):
        if r.method=='GET':
            un=r.GET['usern']
            chk = User.objects.filter(username=un)
            if len(chk)>0:
                return HttpResponse(1)
            else:
                return HttpResponse(0)
def jobpage(request):
    if request.method=="POST":
        pn = request.POST['pname']
        des = request.POST['desc']
        cat_id = request.POST['catg']
        c_obj = Category.objects.get(id=cat_id)
        data = Product(name=pn,cat=c_obj,description=des)
        data.save()
        if'pimage'in request.FILES:
           pc = request.FILES['pimage']
           data.image = pc
           data.save()
    return render(request,'adjob.html',{'cats':all_cat})  
def signout(r):
    logout(r)
    rs = HttpResponseRedirect('/')
    rs.delete_cookie('userid')
    return rs

def dashboard(r):
    usr = Registration.objects.get(user__id=r.user.id)
    return render(r,'dashboard.html',{'profile':usr})    