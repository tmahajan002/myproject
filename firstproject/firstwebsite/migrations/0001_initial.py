# Generated by Django 2.2.1 on 2019-07-16 07:32

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('reg_no', models.IntegerField()),
                ('email', models.EmailField(max_length=254)),
                ('website', models.URLField()),
                ('fee', models.DecimalField(decimal_places=2, max_digits=10)),
                ('dob', models.DateField()),
                ('registred_on', models.DateTimeField(auto_now_add=True)),
                ('approved', models.BooleanField(default=True)),
            ],
        ),
    ]
