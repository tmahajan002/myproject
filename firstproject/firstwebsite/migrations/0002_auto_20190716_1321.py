# Generated by Django 2.2.1 on 2019-07-16 07:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('firstwebsite', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='reg_no',
            field=models.IntegerField(unique=True),
        ),
    ]
