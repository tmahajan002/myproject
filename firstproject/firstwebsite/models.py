from django.db import models

# Create your models here.

class Student(models.Model):
    sub = (
        ('Mr','Mr.'),('Miss','Miss'),
    )
    name = models.CharField(max_length=250,choices=sub)
    reg_no = models.IntegerField(unique=True)
    email = models.EmailField()
    website = models.URLField()
    fee = models.DecimalField(max_digits=10,decimal_places=2)
    dob = models.DateField()
    registred_on = models.DateTimeField(auto_now_add=True)
    approved = models.BooleanField(default=True)
    def __str__(self):
        return str(self.reg_no)+self.name

class Category(models.Model):
    c_name = models.CharField(max_length=250)
    def __str__(self):
        return self.c_name

class Product(models.Model):
    name = models.CharField(max_length=300)
    image = models.ImageField(upload_to="products/%Y/%m/%d")
    cat = models.ForeignKey(Category,on_delete=models.CASCADE)
    description = models.TextField()
    def __str__(self):
        return self.name

from django.contrib.auth.models import User
class Registration(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE)
    contact = models.IntegerField()
    profile_pic = models.ImageField(upload_to='profiles/%Y/%m/%d')
    def __str__(self):
        return self.user.username
        

    