from django.contrib import admin
from firstwebsite.models import Student,Category,Product,Registration


admin.site.site_header = "Myprojectc || SiteName"

class StudentAdmin(admin.ModelAdmin):
   # feilds = ['subtitle','name','email','reg_no']

    list_display = ['id','name','email','reg_no']
    list_filter = ['name','reg_no']
    list_editable = ['name','email']
    search_fields = ['name']

admin.site.register(Student,StudentAdmin)
admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Registration)

# Register your models here.
