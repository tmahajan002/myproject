"""firstproject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from firstwebsite import views

from django.conf import settings
from django.conf.urls.static import static

from rest_framework import serializers,routers,viewsets
from firstwebsite.models import Product,Category

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['name','image','cat','description']
class CategorySerializer(serializers.ModelSerializer):
    products = ProductSerializer(many=True)
    class Meta:
        model = Category
        fields = ['id','c_name']
         

class ProductViewset(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

class CtaegoryViewset(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer



router = routers.DefaultRouter()
router.register('products',ProductViewset)
urlpatterns = [
    path('admin/', admin.site.urls),
    # path('',views.first),
    # path('second/',views.second), 
    path('',views.indexpage,name="hm"),
     path('login/',views.loginpage,name="log"),
     path('student/',views.stdpage,name="st"),
     path('job/',views.jobpage,name="jb"),
     path('resume/',views.resumepage,name="res"),
     path('sign/',views.signup,name="sin"),
     path('dashboard/',views.dashboard,name="dash"),
     path('signout/',views.signout,name="signout"),
     path('stdup/',views.checkUserExist,name="studentup"),
     path('checkUserExist/',views.checkUserExist,name="check"),


     path('api-auth',include('rest_framework.urls'),name="rest_framework"),
     path('api',include(router.urls)),

     
]+static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)

