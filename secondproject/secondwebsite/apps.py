from django.apps import AppConfig


class SecondwebsiteConfig(AppConfig):
    name = 'secondwebsite'
