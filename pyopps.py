class Circle:
    pi = 3.14
    def hello(self):
        return "Fuction Called!!"
    def area(self,r):
        return self.pi*r**2

    #objects
c1 = Circle()
c2 = Circle()
print(c1.pi)
print(c1.hello())
print(c1.area(10))
print(c2.area(100))    