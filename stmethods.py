a = "hello there"
b = "HELLO"
c = "abc123456"
print(a.join("abc"))
print(a.split())
print(a.split('e'))
print(a.index('e',8,10))
print(a.find('z'))
print(b.isupper())
print(b.islower())
print(b.isalpha())
print(b.isalnum())
print(c.isdigit())