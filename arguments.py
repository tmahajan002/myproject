def intro(n,e,r):
    print("Name:{} Email:{} r_no:{}".format(n,e,r))

#positional arguments(order is required)
intro("Peter","ak@gmail.com",1)


#keyword arguments(order is not required)
intro(e="Peter@gmail.com",n="ak",r=1)


#positional argument come first then keyword argument
intro("Aman","aman@gmail.com",r=11)


